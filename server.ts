import * as express from "express";
const needle = require('needle');
const DetectLanguage = require('detectlanguage');
const fs = require('fs');

const tokenjson = require('./api-token.json');

const token = tokenjson["twitterapi"]; //Paste the Twitter API Bearer Token here

const detectLanguage = new DetectLanguage({
    key: tokenjson["languageapi"], //Paste the Language Detection API key here
    ssl: true,
});

let languages;

detectLanguage.languages(function (error, result) {
   languages = result;
});

const app: express.Express = express();
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Our app is running on port ${ PORT }`);
});
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.use("/css", express.static(__dirname + "/style.css"));
app.use("/jquery", express.static(__dirname + "node_modules/jquery/dist"));
app.use("/js", express.static(__dirname + "/client.js"));

app.get("/", (req: express.Request, res: express.Response) => {
    res.status(200);
    res.sendFile(__dirname + "/index.html");
});

app.get("/languages", (req: express.Request, res: express.Response) => {
    res.status(200);
    res.json(languages);
});

app.post("/analyze", (req: express.Request, res: express.Response) => {
    const input: string = req.body.input;
    const count: string = req.body.count;

    if (input.charAt(0) == '@') {
        (async () => {

            try {
                // Make request
                let tweets;
                do {
                    tweets = await getTweetsOfUser(input, count);
                } while (JSON.stringify(tweets) == '[]')
                const response = tweets;
                //Create Array of all texts
                let texts = [];
                for (let item in response) {
                    texts.push(response[item]['full_text']);
                }

                detectLanguage.detect(texts, function(error, result) {
                    console.log(JSON.stringify(result));
                    for (let item in response) {
                        if (JSON.stringify(result[item]) != '[]') {
                            for (let lang in languages) {
                                if (result[item]['0']['language'] == languages[lang]['code']) {
                                    response[item]['detectlanguage'] = languages[lang];
                                }
                            }
                        } else {
                            response[item]['detectlanguage'] = {"name" : "UNDEFINED"};
                        }
                    }
                    console.log(JSON.stringify(response));
                    res.json(response);
                });

            } catch(e) {
                console.log(e);
                res.sendStatus(400);
            }
        })();
    } else {
        (async () => {

            try {
                // Make request
                let response = await getTweetsByQuery(input, count);

                //Create Array of all texts
                let texts = [];
                for (let item in response['data']) {
                    texts.push(response['data'][item]['text']);
                }

                detectLanguage.detect(texts, function(error, result) {
                    for (let item in response['data']) {
                        for (let lang in languages) {
                            if (result[item]['0']['language'] == languages[lang]['code']) {
                                response['data'][item]['detectlanguage'] = languages[lang];
                            }
                        }
                    }
                    res.json(response);
                });

            } catch(e) {
                console.log(e);
                res.sendStatus(400);
            }
        })();
    }
});

async function getTweetsOfUser(input, count) {
    const url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';

    const params = {
        'count' : count,
        'include_rts' : 'false',
        'tweet_mode' : 'extended',
        'screen_name' : input.slice(1, input.length),
    }

    const result = await needle('get', url, params, { headers: {
            "authorization": `Bearer ${token}`
        }})


    if(result.body) {
        return result.body;
    } else {
        throw new Error ('Unsuccessful request')
    }
}

async function getTweetsByQuery(input, count) {
    const url = 'https://api.twitter.com/2/tweets/search/recent';

    const params = {
        'query' : input + " -is:retweet",
        'max_results' : count,
    }

    const result = await needle('get', url, params, { headers: {
            "authorization": `Bearer ${token}`
        }})


    if(result.body) {
        return result.body;
    } else {
        throw new Error ('Unsuccessful request')
    }

}
