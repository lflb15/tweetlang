"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var needle = require('needle');
var DetectLanguage = require('detectlanguage');
var fs = require('fs');
var tokenjson = require('./api-token.json');
var token = tokenjson["twitterapi"]; //Paste the Twitter API Bearer Token here
var detectLanguage = new DetectLanguage({
    key: tokenjson["languageapi"],
    ssl: true,
});
var languages;
detectLanguage.languages(function (error, result) {
    languages = result;
});
var app = express();
var PORT = process.env.PORT || 8080;
app.listen(PORT, function () {
    console.log("Our app is running on port " + PORT);
});
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use("/css", express.static(__dirname + "/style.css"));
app.use("/jquery", express.static(__dirname + "node_modules/jquery/dist"));
app.use("/js", express.static(__dirname + "/client.js"));
app.get("/", function (req, res) {
    res.status(200);
    res.sendFile(__dirname + "/index.html");
});
app.get("/languages", function (req, res) {
    res.status(200);
    res.json(languages);
});
app.post("/analyze", function (req, res) {
    var input = req.body.input;
    var count = req.body.count;
    if (input.charAt(0) == '@') {
        (function () { return __awaiter(void 0, void 0, void 0, function () {
            var tweets, response_1, texts, item, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        tweets = void 0;
                        _a.label = 1;
                    case 1: return [4 /*yield*/, getTweetsOfUser(input, count)];
                    case 2:
                        tweets = _a.sent();
                        _a.label = 3;
                    case 3:
                        if (JSON.stringify(tweets) == '[]') return [3 /*break*/, 1];
                        _a.label = 4;
                    case 4:
                        response_1 = tweets;
                        texts = [];
                        for (item in response_1) {
                            texts.push(response_1[item]['full_text']);
                        }
                        detectLanguage.detect(texts, function (error, result) {
                            console.log(JSON.stringify(result));
                            for (var item in response_1) {
                                if (JSON.stringify(result[item]) != '[]') {
                                    for (var lang in languages) {
                                        if (result[item]['0']['language'] == languages[lang]['code']) {
                                            response_1[item]['detectlanguage'] = languages[lang];
                                        }
                                    }
                                }
                                else {
                                    response_1[item]['detectlanguage'] = { "name": "UNDEFINED" };
                                }
                            }
                            console.log(JSON.stringify(response_1));
                            res.json(response_1);
                        });
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _a.sent();
                        console.log(e_1);
                        res.sendStatus(400);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        }); })();
    }
    else {
        (function () { return __awaiter(void 0, void 0, void 0, function () {
            var response_2, texts, item, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, getTweetsByQuery(input, count)];
                    case 1:
                        response_2 = _a.sent();
                        texts = [];
                        for (item in response_2['data']) {
                            texts.push(response_2['data'][item]['text']);
                        }
                        detectLanguage.detect(texts, function (error, result) {
                            for (var item in response_2['data']) {
                                for (var lang in languages) {
                                    if (result[item]['0']['language'] == languages[lang]['code']) {
                                        response_2['data'][item]['detectlanguage'] = languages[lang];
                                    }
                                }
                            }
                            res.json(response_2);
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        console.log(e_2);
                        res.sendStatus(400);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); })();
    }
});
function getTweetsOfUser(input, count) {
    return __awaiter(this, void 0, void 0, function () {
        var url, params, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
                    params = {
                        'count': count,
                        'include_rts': 'false',
                        'tweet_mode': 'extended',
                        'screen_name': input.slice(1, input.length),
                    };
                    return [4 /*yield*/, needle('get', url, params, { headers: {
                                "authorization": "Bearer " + token
                            } })];
                case 1:
                    result = _a.sent();
                    if (result.body) {
                        return [2 /*return*/, result.body];
                    }
                    else {
                        throw new Error('Unsuccessful request');
                    }
                    return [2 /*return*/];
            }
        });
    });
}
function getTweetsByQuery(input, count) {
    return __awaiter(this, void 0, void 0, function () {
        var url, params, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = 'https://api.twitter.com/2/tweets/search/recent';
                    params = {
                        'query': input + " -is:retweet",
                        'max_results': count,
                    };
                    return [4 /*yield*/, needle('get', url, params, { headers: {
                                "authorization": "Bearer " + token
                            } })];
                case 1:
                    result = _a.sent();
                    if (result.body) {
                        return [2 /*return*/, result.body];
                    }
                    else {
                        throw new Error('Unsuccessful request');
                    }
                    return [2 /*return*/];
            }
        });
    });
}
