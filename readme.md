# TweetLang

## Service der Mashup-App
Die Mashup-App TweetLang bietet die möglichkeit die letzten Tweets anhand von Hashtags, Schlagwörtern oder Accounts abzufragen und die Sprache in welcher der Tweet verfasst wurde zu ermitteln. Außerdem gibt die App eine Übersicht über die Häufigkeit der auftretenden Sprachen anhand eines Diagramms an.

Twitter bietet zwar selbst auch eine Spracherkennung an allerdings umfasst diese gerade einmal 34 Sprachen, TweetLang lässt die Sprachen von einer Language Detection API bestimmen und erkennt so 164 Sprachen.

## Installation

Projekt von GitLab klonen:
```bash
> git clone https://git.thm.de/lflb15/tweetlang.git
```
oder
```bash
> git clone git@git.thm.de:lflb15/tweetlang.git
```

Alle benötigten Node Modules installieren:
```bash
> npm install
```

Einfügen der API-Token:  
Hierzu die auf Moodle mit abgegebene Datei **"api-token.json"** in das Projektverzeichnis ziehen.

Starten des Servers:
```bash
> npm start
```

Nun ist die Webseite unter localhost:8080 zu erreichen.

## Hinweise
Besonders bei großen Abfragen oder bei der Version auf Heroku dauert es teilweise länger bis die Tweets korrekt angezeigt werden. 
Außerdem sind pro Tag nur 1000 Abfragen bei der Language Detection API möglich.

Besonders interessant ist es sich die Sprachverteilung von Hashtags anzuschauen, welche aktuell in den internationalen Nachrichten viel vorkommen. So war während der Entwicklung der App besonders #Beirut und #Minsk sehr lohnenswerte Hashtags.