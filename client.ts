$(function () {
    $("[rel='tooltip']").tooltip();
    // @ts-ignore
    google.charts.load('current', {'packages':['corechart']});
    getSupportedLanguages();
});

function sendInput() {
    $('#loadingbar').css("z-index", "10");
    // @ts-ignore
    $('#loadingbar').LoadingOverlay('show');
    const input = $("#input").val().toString();
    const count = $("#inputNum").val().toString();

    $.ajax("/analyze", {
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            input: input,
            count: count,
        }),
    }).done( (data) => {
        let res = "<thead>" +
            "<tr>" +
                "<th>" +
                    "#" +
                "</th>" +
                "<th class='text-center'>" +
                    "Tweet" +
                "</th>" +
                "<th class='text-center'>" +
                    "Language" +
                "</th>" +
            "</tr>" +
            "</thead><tbody>";
        let langCount = [];
        langCount['total'] = 0;
        if (input.charAt(0) == '@') {
            for (let item in data) {
                res += "<tr><th class='align-middle'>" + (parseInt(item) + 1) + "</th><td class='text-center'><div class='tweet' id='" + data[item]['id_str'] + "'></div></td><td class='text-center align-middle'>" + data[item]['detectlanguage']['name'] +  "</td></tr>"
                if (langCount[data[item]['detectlanguage']['name']] == null) {
                    langCount[data[item]['detectlanguage']['name']] = 1;
                    langCount['total']++;
                } else {
                    langCount[data[item]['detectlanguage']['name']]++;
                    langCount['total']++;
                }
            }
        } else {
            for (let item in data['data']) {
                res += "<tr><th class='align-middle'>" + (parseInt(item) + 1) + "</th><td class='text-center'><div class='tweet' id='" + data['data'][item]['id'] + "'></div></td><td class='text-center align-middle'>" + data['data'][item]['detectlanguage']['name'] +  "</td></tr>"
                if (langCount[data['data'][item]['detectlanguage']['name']] == null) {
                    langCount[data['data'][item]['detectlanguage']['name']] = 1;
                    langCount['total']++;
                } else {
                    langCount[data['data'][item]['detectlanguage']['name']]++;
                    langCount['total']++;
                }
            }
        }
        $("#results").html(res + "</tbody>");
        loadTweets();
        drawChart(langCount);
        // @ts-ignore
        $('#loadingbar').LoadingOverlay('hide');
        $('#loadingbar').css("z-index", "-1");
    }).catch( (jqXHR) => {
        console.log(jqXHR);
    });
}

function loadTweets() {
    let tweets = $(".tweet");

    $(tweets).each( function (t, tweet) {
        let id = $(this).attr('id');

        // @ts-ignore
        twttr.widgets.createTweet(
            id, tweet,
            {
                conversation : 'none',    // or all
                cards        : 'hidden',  // or visible
                align        : 'center', // default is blue
                theme        : 'light',    // or dark
                dnt          : 'true',
            }
        )
    })
}

function drawChart(langCount) {
    //generate array for pie
    let langs = [];
    langs.push(['Language', 'Count'])
    for (let item in langCount){
        if (item != 'total') {
            langs.push([item, langCount[item]])
        }
    }

    // @ts-ignore
    var data = google.visualization.arrayToDataTable(langs);

    var options = {
        title: 'Languages in ' + langCount['total'] + ' tweets'
    };

    $('#piechart').css({"width":"auto","height":"350px"});

    // @ts-ignore
    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
}

function getSupportedLanguages() {
    $.ajax("/languages", {
        type: "GET",
    }).done( (data) => {
        let langs = "";

        for (let item in data) {
            langs += "<span class=\"badge badge-pill badge-light m-2\">" + data[item]['name'] + "</span>"
        }

        $("#supportedLanguages").html(langs);
    }).catch( (jqXHR) => {
        console.log(jqXHR);
    });
}